#!/bin/bash
seconds=600
end=$((SECONDS+seconds))
wait_in_minutes="$((seconds/60)) minutes $((seconds%60)) secondes"
code=503

echo "$wait_in_minutes"

if [ -z "$BASE_URL" ]; then
  echo "ATTENTION : L'url n'as pas été définie ! Vous devez donner une url" 1>&2
  exit 1
fi

echo "BASE_URL --> $BASE_URL"

while :
do
  code=$(curl -kLo /dev/null -s -w "%{http_code}" "$BASE_URL")
  messageError="
  Error ! - status code : '$code'.
  Le status code doit appartenir aux codes '2xx - Succès' pour être validé
  Ou être égale à 503 - (Service Temporairement indisponible) pour lancer de nouvelles tentatives"

  if [[ $code =~ 40[0-9] ]]; then
    echo "$messageError" 1>&2
    exit 1
  elif [[ $code =~ 20[0-9] ]]; then
    echo "success $code"
    exit 0
  elif [ "$code" != 503 ]; then
    echo "$messageError" 1>&2
    exit 1
  elif [ $SECONDS -gt $end ]; then
    echo "L'url n'a pas été valide pendent $wait_in_minutes " 1>&2
    exit 1
  fi

  sleep 15
done
