const https = require('https');
const fs = require('fs');


const {
  PROJECT_ID = "35140662",
  ENV_NAME = "staging",
  GITLAB_TOKEN
} = process.env

if(!PROJECT_ID || !ENV_NAME || !GITLAB_TOKEN ){
  console.error(`WARNING : Missing environment variables !
  GITLAB_TOKEN --> ${ GITLAB_TOKEN  ? 'OK' : 'MISSING'}
  PROJECT_ID --> ${ PROJECT_ID ? 'OK' : 'MISSING'}
  ENV_NAME --> ${ENV_NAME  ? 'OK' : 'MISSING'}
  `)
  process.exit(1)
}


const options = {
  hostname: 'gitlab.com',
  protocol: "https:",
  port: 443,
  path: `/api/v4/projects/${PROJECT_ID}/environments?name=${ENV_NAME}`,
  method: 'GET',
  headers: {
    'PRIVATE-TOKEN': GITLAB_TOKEN,
  }
};

const req = https.get(options, (res) => {
  res.setEncoding('utf8');
  let rawData = '';

  // A chunk of data has been received.
  res.on('data', (chunk) => {
    rawData += chunk;
  });

  res.on('end', () => {
    const data = JSON.parse(rawData)
    console.log(data)
    const url = data[0].external_url
    fs.writeFileSync('url.env',`ENV_URL=${url}`)
    console.log(`Success ENV_URL=${url}`)
    process.exit(0)
  });

})

req.on("error", (err) => {
  console.error("Error: " + err.message);
  process.exit(1)
});
